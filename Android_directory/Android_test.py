import os
import unittest
import time
from appium import webdriver

from selenium.webdriver import DesiredCapabilities, ActionChains

driver = webdriver


class Automation_android(unittest.TestCase):

    menu_tab = "//*[@resource-id='com.omniocr:id/Image_view_menu']"
    home_button = "//*[@text='Home']"
    self_assessment = "//*[@text='Start new assessment']"
    camera_icon = "//*[@resource-id='com.omniocr:id/gallery_iv']"
    select_first_image = "(//*[@resource-id='com.omniocr:id/imageView'])[1]"
    select_second_image = "(//*[@resource-id='com.omniocr:id/imageView'])[2]"
    select_image_ok_button = "//*[@resource-id='com.omniocr:id/selection_ok']"
    capture_button = "//*[@resource-id='com.omniocr:id/clickme']"
    gallery_button = "//*[@resource-id='com.omniocr:id/clickme_gallary']"
    next_button = "//*[@resource-id='com.omniocr:id/text_access']"
    select_image = "//*[@index='1']"
    select_image2 = "//*[@index='2']"
    send_document = "//*[@resource-id='com.omniocr:id/button_next']"
    results_data1 = "//*[@resource-id='com.omniocr:id/text1' or @resource-id='com.omniocr:id/text2']"
    ar = "//*[@resource-id='com.omniocr:id/text_ar']"
    form_1 = "//*[@resource-id='com.omniocr:id/image_list_rv']/child::*[1]"
    delete = "//*[@resource-id='com.omniocr:id/delete']"
    cancel = "//*[@text='Cancel']"

    select = "//*[@text='Select']"
    select_all = "//*[@text='Select All']"
    recapture_button = "//*[@resource-id='com.omniocr:id/re_capture']"
    click_here = "//*[@resource-id='com.omniocr:id/text1']"
    required_attention = "//*[@resource-id='com.omniocr:id/required_attention']"

    delete_old_files = "//*[@text='Delete old files']"
    gallery_images_1 = "(//*[@resource-id='com.omniocr:id/check_box'])[1]"
    gallery_images_2 = "(//*[@resource-id='com.omniocr:id/check_box'])[2]"
    gallery_images_delete = "//*[@resource-id='com.omniocr:id/delete_button']"
    gallery_images_back_button = "//*[@resource-id='com.omniocr:id/back_button']"

    def __init__(self, driver):
        super().__init__()
        self.driver = driver

    def set_desired_capabilities(self):
        PATH = lambda p: os.path.abspath(
            os.path.join(os.path.dirname(__file__), p)
        )
        dc = DesiredCapabilities.ANDROID
        dc["deviceName"] = "Redmi Note 7S"
        dc["platformVersion"] = "9"
        dc["platformName"] = "Android"
        dc["noReset"] = "true"
        dc["appPackage"] = "com.omniocr"
        dc["appActivity"] = ".MainActivity"
        dc["browserName"] = ""
        dc["automationName"] = "UiAutomator2"
        self.driver = webdriver.Remote("http://localhost:4723/wd/hub", dc)

    def click_menu_tab(self):
        time.sleep(2)
        self.driver.find_element_by_xpath(self.menu_tab).click()

    def click_home_button(self):
        time.sleep(2)
        self.driver.find_element_by_xpath(self.home_button).click()

    def click_start_new_assessment(self):
        time.sleep(2)
        verify_start_button = self.driver.find_element_by_xpath(self.self_assessment).text
        self.assertTrue(verify_start_button == "Start new assessment", "Start New Assessment button is not visible")
        self.driver.find_element_by_xpath(self.self_assessment).click()

    def click_camera_icon(self):
        time.sleep(2)
        self.driver.find_element_by_xpath(self.camera_icon).click()

    def click_and_select_image(self):

        time.sleep(3)
        image_element = self.driver.find_element_by_xpath(self.select_first_image)
        actions1 = ActionChains(self.driver)
        actions1.click_and_hold(image_element)
        actions1.perform()
        time.sleep(3)

    def click_and_select_second_image(self):
        time.sleep(1)
        image_element = self.driver.find_element_by_xpath(self.select_second_image)
        actions1 = ActionChains(self.driver)
        actions1.click_and_hold(image_element)
        actions1.perform()

    def click_select_image_ok(self):
        time.sleep(1)
        self.driver.find_element_by_xpath(self.select_image_ok_button).click()

    def click_capture_button(self):
        time.sleep(3)
        self.driver.find_element_by_xpath(self.capture_button).click()
        time.sleep(1)

    def click_next_button(self):
        time.sleep(2)
        self.driver.find_element_by_xpath(self.next_button).click()
        time.sleep(4)

    def click_next_button1(self):
        time.sleep(2)
        self.driver.find_element_by_xpath(self.next_button).click()
        time.sleep(2)

    def click_gallery(self):
        time.sleep(2)
        self.driver.find_element_by_xpath(self.gallery_button).click()

    def click_select_image(self):
        time.sleep(2)
        self.driver.find_element_by_xpath(self.select_image).click()

    def click_select_image2(self):
        time.sleep(2)
        self.driver.find_element_by_xpath(self.select_image2).click()

    def click_send_document(self):
        time.sleep(1)
        self.driver.find_element_by_xpath(self.send_document).click()

    def get_results_text(self):
        time.sleep(7)
        self.driver.implicitly_wait(90)
        validate_keys = self.driver.find_element_by_xpath(self.required_attention).text
        self.assertTrue(validate_keys == "Required attention", "User didn't landed on Key value screen")
        data_list1 = []
        time.sleep(2)
        result_data = self.driver.find_elements_by_xpath(self.results_data1)
        for data in result_data:
            data_list = data.text
            data_list1.append(data_list)
        time.sleep(2)

        #self.driver.back()
        if data_list1.__contains__('112   105   108'):
            print('......................its there')
        print("Data List1:")
        print(data_list1)
        time.sleep(2)
        self.driver.swipe(start_x=75, start_y=500, end_x=75, end_y=0, duration=800)

    def click_ar(self):
        time.sleep(4)
        self.driver.implicitly_wait(100)
        self.driver.find_element_by_xpath(self.ar).click()
        time.sleep(7)

    def select_a_form(self):
        time.sleep(2)
        self.driver.find_element_by_xpath(self.form_1).click()

    def delete_a_form(self):
        time.sleep(2)
        self.driver.find_element_by_xpath(self.delete).click()
        time.sleep(2)
        self.driver.find_element_by_xpath(self.cancel).click()
        time.sleep(2)

    def click_select_button(self):
        self.driver.implicitly_wait(100)
        self.driver.find_element_by_xpath(self.select).click()

    def click_select_all_button(self):
        self.driver.implicitly_wait(100)
        self.driver.find_element_by_xpath(self.select_all).click()

    def click_recapture_button(self):
        self.driver.find_element_by_xpath(self.recapture_button).click()

    def delete_all_forms(self):
        self.driver.implicitly_wait(100)
        self.driver.find_element_by_xpath(self.delete).click()
        self.driver.implicitly_wait(100)
        self.driver.find_element_by_xpath(self.click_here).click()
        time.sleep(1)
        self.driver.back()
        self.driver.back()

    def delete_forms(self):
        self.driver.implicitly_wait(100)
        self.driver.find_element_by_xpath(self.delete).click()
        self.driver.implicitly_wait(100)
        self.driver.find_element_by_xpath(self.click_here).click()
        time.sleep(1)

    def click_delete_old_files_tab(self):
        self.driver.implicitly_wait(100)
        self.driver.find_element_by_xpath(self.delete_old_files).click()
        time.sleep(1)

    def select_image_1_gallery_images(self):
        self.driver.find_element_by_xpath(self.gallery_images_1).click()

    def select_image_2_gallery_images(self):
        self.driver.find_element_by_xpath(self.gallery_images_2).click()

    def click_delete_button_gallery_images(self):
        time.sleep(1)
        self.driver.find_element_by_xpath(self.gallery_images_delete).click()
        time.sleep(1)

    def click_back_button_gallery_images(self):
        time.sleep(1)
        self.driver.find_element_by_xpath(self.gallery_images_back_button).click()
        time.sleep(1)







# a= Automation_android()
# a.Set_desired_capabilities()
# a.click_menu_tab()
# a.click_home_button()
# a.click_Start_new_assesment()
# a.click_capture_button()
# a.click_next_button()
# a.click_send_document()
# a.get_results_text()

