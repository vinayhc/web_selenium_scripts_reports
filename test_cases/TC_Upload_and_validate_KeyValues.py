from page_objects.Groups_data_page import Test_Groups_page
from page_objects.Login_page import Test_Login_page

import unittest
import HtmlTestRunner
from selenium import webdriver
import os
import sys
import json
sys.path.append("D:/test")




class Form_upload(unittest.TestCase):

    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
    DRIVER_BIN = os.path.join(PROJECT_ROOT, "../drivers/chromedriver")
    driver = webdriver.Chrome(executable_path=DRIVER_BIN)
    baseURL = "https://megaocr-ui-rgacf-daiichi.omni.sc"
    file1 = open("../data/Login_credentials.json", 'r')
    json_input = file1.read()
    inputBody = json.loads(json_input)
    json_data = list(inputBody.values())
    username = json_data.__getitem__(0)
    password = json_data.__getitem__(1)

    @classmethod
    def setUpClass(cls):
        cls.driver.get(cls.baseURL)        # Opens the Web Application
        cls.driver.maximize_window()       # Maximize the window
        cls.driver.implicitly_wait(100)

    def test_b_login(self):
        lp = Test_Login_page(self.driver)  # Please Change the username and password in Login_credentials.json file
        lp.enter_user_name(self.username)  # Enters the username
        self.driver.implicitly_wait(10)
        lp.enter_password(self.password)   # Enters the password
        lp = Test_Login_page(self.driver)
        self.driver.implicitly_wait(10)
        lp.click_submit_button()           # Clicks submit button

    def test_c_upload_form(self):
        gp = Test_Groups_page(self.driver)
        gp.click_a_groups_tab()            # Clicks on the Groups tab
        gp.click_automation_group()        # Selects one of the Groups
        gp.click_h_click_create_form()     # Clicks on 'Create Form' button
        gp.upload_i_form()                 # Uploads the form in to the application
        gp.enter_form_provider()           # Enters the Form provider name
        gp.click_save_button()             # Clicks on save  button

    def test_d_compare_results(self):
        gp = Test_Groups_page(self.driver)
        gp.click_c_first_form()             # Selects the uploaded form
        gp.click_d_actions_drop_down()      # Clicks on Action drop down
        gp.click_e_view_overall_results()   # Clicks on 'View overall results
        gp.get_f_list_of_form_values()      # Gets the Results in to a List and Compare with Form values


# Generates HTML Test Report
if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(
        output='D:/Automation - BitBucket/test/Omniscience/reports'))