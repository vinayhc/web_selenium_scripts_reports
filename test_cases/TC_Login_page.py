from page_objects.Login_page import Test_Login_page
import unittest
import HtmlTestRunner
from selenium import webdriver
import os
import sys
import time
import json
sys.path.append("D:/test")


class Login_test(unittest.TestCase):
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
    DRIVER_BIN = os.path.join(PROJECT_ROOT, "../drivers/chromedriver")
    driver = webdriver.Chrome(executable_path=DRIVER_BIN)
    baseURL = "https://gtlite.omni.sc"
    file1 = open("../data/Login_credentials.json", 'r')
    json_input = file1.read()
    inputBody = json.loads(json_input)
    json_data = list(inputBody.values())
    username = json_data.__getitem__(0)
    password = json_data.__getitem__(1)

    @classmethod
    def setUpClass(cls):      # Opens th Web application
        cls.driver.get(cls.baseURL)
        cls.driver.maximize_window()
        cls.driver.implicitly_wait(10)
        time.sleep(5)

    def test_login(self):    # Login to the Web application
        lp = Test_Login_page(self.driver)
        lp.enter_user_name(self.username)
        self.driver.implicitly_wait(10)
        lp.enter_password(self.password)
        self.driver.implicitly_wait(10)
        lp.click_submit_button()


# Generates HTML Test Report
if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(
        output='D:/test/reports'))
