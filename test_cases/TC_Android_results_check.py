import unittest
import HtmlTestRunner
from selenium import webdriver
import os
import sys
from Android_directory.Android_test import Automation_android, driver


class Android_Runner_class1(unittest.TestCase):
    auto_android = Automation_android(driver)

    @classmethod
    def setUpClass(cls):
        print("test starts...")

    def test_a_desired_capabilities(self):

        self.auto_android.set_desired_capabilities()

    def test_b_capture_image_start__new_assessment(self):

        self.auto_android.click_menu_tab()
        self.auto_android.click_home_button()
        self.auto_android.click_start_new_assessment()
        self.auto_android.click_capture_button()
        self.auto_android.click_next_button()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()





if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(
        output='D:/Automation/Omni_Test_Automation/reports'))